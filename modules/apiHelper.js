'use strict';

const request = require('request');

let apiHelper = {
	getApiData: (callback) => {
		request({url: 'http://skateipsum.com/get/3/1/JSON'}, (err, response, body) => {
			if (err) {
				callback(err);
			} else {
				let json = JSON.parse(body);
				callback(null, json);
			}
		});
	}
};

module.exports = apiHelper;