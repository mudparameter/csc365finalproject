'use strict';

require('dotenv').config();

const express = require('express'),
	app = express(),
	passport = require('passport'),
	expressSession = require('express-session'),
	TwitterStrategy = require('passport-twitter').Strategy,
	bodyParser = require('body-parser');

const tweetHelper = require('./tweetHelper');
const apiHelper = require('./apiHelper');

// configure the session
app.use(expressSession({
	secret: 'higglebiggle',
	saveUninitialized: false,
	resave: false,
}));

// configure the twitterstrategy
passport.use(new TwitterStrategy({
	consumerKey: process.env.clientID,
	consumerSecret: process.env.clientSecret,
	callbackURL: 'http://localhost:3000/auth/twitter/callback'
},
(token, tokenSecret, profile, done) => {
	return done(null, profile);
}
));

// this method is required by passport even though we arent actually doing anything with it
passport.serializeUser(function(user, cb) { 
	cb(null, user);
});
  
// this method is required by passport even though we arent actually doing anything with it
passport.deserializeUser(function(obj, cb) {
	cb(null, obj);
});

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function ensureAuthenticated(req, res, next) {
	if (req.isAuthenticated()) {
		next();
	} else {
		console.log('need to log in!!!');
		res.render('login');
	}
}

app.use(express.static('resources'));

app.set('view engine', 'pug');
app.set('views', 'views');

// MARK twitter auth methods
app.get('/auth/twitter', 
	passport.authenticate('twitter')); // authenticate with twitter

app.get('/auth/twitter/callback', 
	passport.authenticate('twitter', { failureRedirect: '/login' }),
	(req, res) => {
		res.redirect('/');
	}
); // if creds fail, redirect to login, otherwise redirect to home as user

// MARK endpoints
app.get('/', ensureAuthenticated, (req, res) => {
	res.render('home', {username: req.user.displayName});
});

app.get('/about', ensureAuthenticated, (req, res) => {
	res.render('about');
});

app.get('/contact', ensureAuthenticated, (req, res) => {
	res.render('contact');
});

app.post('/tweet', (req, res) => {
	console.log(req.body);
	try {
		tweetHelper.sendTweet(truncateString(req.body.tweetText));
		res.json(generateResponseObject('success', 'tweet posted'));
	} catch (err) {
		res.json(generateResponseObject('error', err));
	}
});

app.get('/apiData', (req, res) => {
	try {
		apiHelper.getApiData((err, json) => {
			if (err) {
				throw 'error';
			}
			res.json(generateResponseObject('success', json));
		});
	} catch (err) {
		res.json(generateResponseObject('error', 'an error occurred :('));
	}
});

// MARK helper functions
function generateResponseObject(type, msg) {
	return {'type': type, 'message': msg};
}

function truncateString(string, length) {
	if (string.length > length - 1) {
		return string.substring(0, length);
	}
	return string;
}

module.exports = app;