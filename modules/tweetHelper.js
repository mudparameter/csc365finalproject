'use strict';

require('dotenv').config();

const Twit = require('twit');

const tweeter = new Twit({
	consumer_key: process.env.clientID,
	consumer_secret: process.env.clientSecret,
	access_token: process.env.accessToken,
	access_token_secret: process.env.accessTokenSecret
});

const helper = {
	sendTweet: (text) => {
		tweeter.post('statuses/update', {
			status: text
		}),
		(err) => {
			if (err) {
				throw 'an error occurred';
			}
		};
	}
};

module.exports = helper;