'use strict';

require('dotenv').config();

const server = require('./modules/appConfig');

server.listen(process.env.port, () => {
	console.log(`server listening on port ${process.env.port}`);
});