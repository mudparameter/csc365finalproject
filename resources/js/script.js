'use strict';

document.addEventListener('DOMContentLoaded', () => {
	document.getElementById('getSkaterIpsumSubmit').addEventListener('click', () => {
		getSkaterIpsum((err, response) => {
			if (err) {
				console.log(err);
				setErrStatus('an error occurred getting from api. please try again later :(');
				return;
			}
			setErrStatus('');
			document.getElementById('ipsumTextArea').value = response;
		});
	});

	document.getElementById('tweetSubmit').addEventListener('click', () => {
		sendTweet((err) => {
			if (err) {
				console.log(err);
				setErrStatus('An error occurred posting the tweet. please try again later :(');
				return;
			}
			setSuccessStatus('Tweet posted!!');
		});
	});
});

function setErrStatus(string) {
	document.getElementById('statusSuccess').textContent = '';
	document.getElementById('statusErr').textContent = string;
}

function setSuccessStatus(string) {
	document.getElementById('statusErr').textContent = '';
	document.getElementById('statusSuccess').textContent = string;
}

function truncateString(string, length) {
	if (string.length > length - 1) {
		return string.substring(0, length);
	}
	return string;
}

function getSkaterIpsum(callback) {
	fetch('http://localhost:3000/apiData', {
		method: 'GET',
		headers: {
			'Content-type': 'application/json'
		}
	}).then((res) => {
		return res.json();
	}).then((response) => { 
		if (response.type === 'error') {
			throw 'an error occurred';
		} else {
			callback(null, response.message);
		}
	}).catch((error) => {
		callback(error);
	});
}

function sendTweet(callback) {
	let status = truncateString(document.getElementById('ipsumTextArea').value, 160);
	if (status.length === 0 || status === undefined || status === null) {
		callback('textarea cannot be empty');
		return;
	}

	let data = {
		tweetText: status
	};

	fetch('http://localhost:3000/tweet', {
		method: 'POST',
		body: JSON.stringify(data),
		headers: {
			'Content-type': 'application/json'
		}
	}).then((res) => {
		return res.json();
	}).then((response) => { 
		if (response.type === 'error') {
			throw 'an error occurred';
		} else {
			callback(null, response.message);
		}
	}).catch((error) => {
		callback(error);
	});
}